# PR-Line CRM Front-End Part
**The main concepts:**

- Use feature branches for all new features and bug fixes.
- Merge feature branches into the dev (main) branch using merge requests.
- Keep a high quality, up-to-date dev branch.
- Master branch is meant for deploy-ready versions.

**Use feature branches for your work**

Develop your features and fix bugs on additional feature branches based on the dev branch. Feature branches isolate work in progress from the completed work on the dev branch. Even small fixes and changes should have their own feature branch.

**Convention of naming the feature branches**

- bugfix/description (if a bug is being fixed)
- feature/feature-name (if a new feature is being developed/implemented)

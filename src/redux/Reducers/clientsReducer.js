import {
  CLIENTS_CLOSE_FORM,
  CLIENTS_FETCH_DATA,
  CLIENTS_FORM_FETCH_DATA,
  CLIENTS_GET_CONCRETE_CLIENT_DATA,
  CLIENTS_OPEN_FORM,
  CLIENTS_STORE_SELECTED_CLIENT_DATA,
  CONTACT_PERSONS_CLOSE_ADDFORM,
  CONTACT_PERSONS_FORM_FETCH_DATA,
  CONTACT_PERSONS_OPEN_ADDFORM,
  FETCH_CONCRETE_CLIENT_DATA,
  MANAGERS_CLOSE_ADDFORM,
} from '../actionTypes'
import { showAlert } from '../Actions/appActions'
import { ERROR_ALERT } from '../../components/Alerts/alertTypes'

const initialState = {
  clientsList: [],
  loading: true,
  formToOpenType: '',
  formTitle: '',
  formAlertText: '',
  formActionButton: '',
  clientFormIsOpen: false,
  clientTypes: undefined,
  clientSources: undefined,
  managersList: undefined,
  contactPersonFormIsOpen: false,
  chosenClientId: undefined,
  chosenClientName: undefined,
  concreteClientData: undefined,
  contactPersonStatuses: undefined,
}

export const clientsReducer = (state = initialState, action) => {
  switch (action.type) {
    case CLIENTS_FETCH_DATA:
      if (action.payload) {
        return {
          ...state,
          clientsList: action.payload,
          loading: false,
        }
      } else {
        return state
      }
    case CLIENTS_GET_CONCRETE_CLIENT_DATA:
      // debugger
      return {
        ...state,
        concreteClientData: action.payload,
      }
    case CLIENTS_OPEN_FORM:
      return {
        ...state,
        clientFormIsOpen: true,
        formToOpenType: action.formType,
        formTitle: action.formTitle,
        formAlertText: action.formAlertText,
        formActionButton: action.formActionButton,
      }
    case CLIENTS_CLOSE_FORM:
      return {
        ...state,
        clientFormIsOpen: false,
      }
    case CLIENTS_FORM_FETCH_DATA:
      return {
        ...state,
        clientTypes: action.clientTypes,
        clientSources: action.clientSources,
      }
    case CLIENTS_STORE_SELECTED_CLIENT_DATA:
      return {
        ...state,
        chosenClientId: action.payload.id,
        chosenClientName: action.payload.clientName,
      }
    case CONTACT_PERSONS_OPEN_ADDFORM:
      return {
        ...state,
        contactPersonFormIsOpen: true,
      }
    case CONTACT_PERSONS_CLOSE_ADDFORM:
      return {
        ...state,
        contactPersonFormIsOpen: false,
      }
    case CONTACT_PERSONS_FORM_FETCH_DATA:
      return {
        ...state,
        contactPersonStatuses: action.payload,
      }
    default:
      return state
  }
}

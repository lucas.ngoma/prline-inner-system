import { CONTRACTORS_FETCH_DATA } from '../actionTypes'

const initialState = {
  contractorsList: [],
  loading: true,
}

export const contractorsReducer = (state = initialState, action) => {
  switch (action.type) {
    case CONTRACTORS_FETCH_DATA:
      if (action.payload) {
        return {
          ...state,
          contractorsList: action.payload,
          loading: false,
        }
      } else {
        return state
      }
    default:
      return state
  }
}

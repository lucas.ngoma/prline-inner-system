import {
  MANAGERS_FETCH_DATA,
  MANAGERS_OPEN_FORM,
  MANAGERS_CLOSE_FORM,
  MANAGERS_STORE_SELECTED_MANAGER_DATA,
} from '../actionTypes'

const initialState = {
  managersList: [],
  loading: true,
  passwordValidationIsRequired: undefined,
  formToOpenType: '',
  formTitle: '',
  formAlertText: '',
  formActionButton: '',
  managerFormIsOpen: false,
  initialLastName: '',
  initialFirstName: '',
  initialMiddleName: '',
  initialEmail: '',
  initialPhone: '',
  initialPassword: '',
  chosenManagerId: undefined,
  chosenManagerLastName: undefined,
  chosenManagerFirstName: undefined,
  chosenManagerMiddleName: undefined,
  chosenManagerEmailName: undefined,
  chosenManagerActive: undefined,
}

export const managersReducer = (state = initialState, action) => {
  switch (action.type) {
    case MANAGERS_FETCH_DATA:
      if (action.payload) {
        return {
          ...state,
          managersList: action.payload,
          loading: false,
        }
      } else {
        return state
      }

    case MANAGERS_OPEN_FORM:
      return {
        ...state,
        managerFormIsOpen: true,
        formToOpenType: action.formType,
        formTitle: action.formTitle,
        formAlertText: action.formAlertText,
        formActionButton: action.formActionButton,
      }

    case MANAGERS_CLOSE_FORM:
      return {
        ...state,
        managerFormIsOpen: false,
      }

    case MANAGERS_STORE_SELECTED_MANAGER_DATA:
      return {
        ...state,
        chosenManagerId: action.payload.id,
        chosenManagerLastName: action.payload.lastName,
        chosenManagerFirstName: action.payload.firstName,
        chosenManagerMiddleName: action.payload.middleName,
        chosenManagerEmailName: action.payload.email,
        chosenManagerPhone: action.payload.phone,
        chosenManagerActive: action.payload.enable,
      }

    default:
      return state
  }
}

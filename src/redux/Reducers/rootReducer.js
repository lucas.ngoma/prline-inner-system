import { combineReducers } from 'redux'
import { authReducer } from './authReducer'
import { appReducer } from './appReducer'
import { managersReducer } from './managersReducer'
import { clientsReducer } from './clientsReducer'
import { contractorsReducer } from './contractorsReducer'

export const rootReducer = combineReducers({
  wholeApplication: appReducer,
  authentication: authReducer,
  managersList: managersReducer,
  clientsList: clientsReducer,
  contractorsList: contractorsReducer,
})

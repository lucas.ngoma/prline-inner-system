import { INFO_ALERT } from '../../components/Alerts/alertTypes'
import { showAlert } from '../Actions/appActions'
import { LOGIN, LOGOUT } from '../actionTypes'

let isAuth =
  localStorage.getItem('accessToken') && localStorage.getItem('userType')
    ? true
    : false
let authKey =
  localStorage.getItem('accessToken') && localStorage.getItem('userType')
    ? localStorage.getItem('accessToken')
    : ''
let userType =
  localStorage.getItem('accessToken') && localStorage.getItem('userType')
    ? localStorage.getItem('userType')
    : ''

const initialState = {
  isAuth,
  authKey,
  userType,
}

export const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN:
      debugger
      if (localStorage.getItem('accessToken') === action.payload.access_token) {
        return {
          ...state,
          isAuth: true,
          authKey: localStorage.getItem('accessToken'),
          userType: action.payload.role,
        }
      } else {
        return state
      }
    case LOGOUT:
      localStorage.removeItem('accessToken')
      localStorage.removeItem('userType')
      return {
        ...state,
        isAuth: false,
        authKey: '',
        userType: '',
      }
    default:
      return state
  }
}

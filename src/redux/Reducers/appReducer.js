import {
  ERROR_ALERT,
  INFO_ALERT,
  SUCCESS_ALERT,
  WARNING_ALERT,
} from '../../components/Alerts/alertTypes'
import {
  SHOW_ALERT,
  HIDE_ALERT,
  STORE_CURRENT_LOCATION,
  SHOW_LOGOUT_DIALOG,
  HIDE_LOGOUT_DIALOG,
} from '../actionTypes'

const initialState = {
  loader: false,
  errorAlertIsOpen: false,
  infoAlertIsOpen: false,
  successAlertIsOpen: false,
  warningAlertIsOpen: false,
  alertCode: null,
  alertText: null,
  logOutDialogIsOpen: false,
  currentRoute: undefined,
}

export const appReducer = (state = initialState, action) => {
  switch (action.type) {
    case SHOW_ALERT:
      debugger
      switch (action.alertType) {
        case ERROR_ALERT:
          debugger
          return {
            ...state,
            errorAlertIsOpen: true,
            alertCode: action.payload.status,
            alertText: action.payload.data.errorText,
          }

        case INFO_ALERT:
          return {
            ...state,
            infoAlertIsOpen: true,
            alertCode: action.payload.status,
            alertText: action.payload.data.errorText,
          }

        case SUCCESS_ALERT:
          return {
            ...state,
            successAlertIsOpen: true,
            alertCode: action.payload.status,
            alertText: action.payload.data.infoText,
          }
        case WARNING_ALERT:
          return {
            ...state,
            warningAlertIsOpen: true,
            alertCode: action.payload.status,
            alertText: action.payload.data.infoText,
          }
        default:
          return state
      }

    case HIDE_ALERT:
      return {
        ...state,
        errorAlertIsOpen: false,
        infoAlertIsOpen: false,
        successAlertIsOpen: false,
        warningAlertIsOpen: false,
      }

    case SHOW_LOGOUT_DIALOG:
      return {
        ...state,
        logOutDialogIsOpen: true,
      }

    case HIDE_LOGOUT_DIALOG:
      return {
        ...state,
        logOutDialogIsOpen: false,
      }

    case STORE_CURRENT_LOCATION:
      return {
        ...state,
        currentRoute: action.payload.route,
      }
    default:
      return state
  }
}

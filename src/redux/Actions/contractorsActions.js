import axios from 'axios'
import {
  ERROR_ALERT,
  INFO_ALERT,
  SUCCESS_ALERT,
} from '../../components/Alerts/alertTypes'
import { CONTRACTORS_FETCH_DATA, LOGOUT } from '../actionTypes'
import { showAlert } from './appActions'

export function fetchContractorsData() {
  debugger
  return async (dispatch) => {
    try {
      const response = await axios.get('/ContractorsList', {
        headers: {
          Accept: 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('accessToken'),
        },
      })
      // console.log(response.data)
      dispatch({ type: CONTRACTORS_FETCH_DATA, payload: response.data })
    } catch (error) {
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.log(error.response.data)
        console.log(error.response.status)
        console.log(error.response.headers)
        if (error.response.status === 401) {
          dispatch({ type: LOGOUT })
          dispatch(showAlert(error.response, INFO_ALERT))
        }
        // dispatch(showAlert(error.response))
      } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        console.log(error.request)
      } else {
        // Something happened in setting up the request that triggered an Error
        console.log('Error', error.message)
      }
      // console.log(error.config)
    }
  }
}

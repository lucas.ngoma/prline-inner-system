import {
  HIDE_ALERT,
  HIDE_LOGOUT_DIALOG,
  SHOW_ALERT,
  SHOW_LOGOUT_DIALOG,
  STORE_CURRENT_LOCATION,
} from '../actionTypes'

export function showAlert(text, alertType) {
  return (dispatch) => {
    dispatch({
      type: SHOW_ALERT,
      alertType,
      payload: text,
    })
    debugger
    setTimeout(() => {
      dispatch(hideAlert())
    }, 7000)
  }
}

export function hideAlert() {
  return {
    type: HIDE_ALERT,
  }
}

export function showLogOutDialog() {
  return {
    type: SHOW_LOGOUT_DIALOG,
  }
}

export function hideLogOutDialog() {
  return {
    type: HIDE_LOGOUT_DIALOG,
  }
}

export function storeCurrentPageRoute(route) {
  return {
    type: STORE_CURRENT_LOCATION,
    payload: route,
  }
}

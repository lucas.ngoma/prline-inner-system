import axios from 'axios'
import {
  ERROR_ALERT,
  INFO_ALERT,
  SUCCESS_ALERT,
} from '../../components/Alerts/alertTypes'
import {
  LOGOUT,
  MANAGERS_CLOSE_FORM,
  MANAGERS_FETCH_DATA,
  MANAGERS_OPEN_FORM,
  MANAGERS_STORE_SELECTED_MANAGER_DATA,
} from '../actionTypes'
import { showAlert } from './appActions'

export function fetchManagersData() {
  return async (dispatch) => {
    try {
      const response = await axios.get('/ManagersList', {
        headers: {
          Accept: 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('accessToken'),
        },
      })
      // console.log(response)
      dispatch({ type: MANAGERS_FETCH_DATA, payload: response.data })
    } catch (error) {
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.log(error.response.data)
        console.log(error.response.status)
        console.log(error.response.headers)
        if (error.response.status === 401) {
          dispatch({ type: LOGOUT })
          dispatch(showAlert(error.response, INFO_ALERT))
        }
        // dispatch(showAlert(error.response))
      } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        console.log(error.request)
      } else {
        // Something happened in setting up the request that triggered an Error
        console.log('Error', error.message)
      }
      // console.log(error.config)
    }
  }
}

export function managerFormOpen(
  formType,
  formTitle,
  formAlertText,
  formActionButton,
) {
  return {
    type: MANAGERS_OPEN_FORM,
    formType,
    formTitle,
    formAlertText,
    formActionButton,
  }
}

export function managerFormClose() {
  return {
    type: MANAGERS_CLOSE_FORM,
  }
}

export function addManagerIntoDatabase(managerData) {
  return async (dispatch) => {
    try {
      const response = await axios.post('/ManagersList', managerData, {
        headers: {
          Accept: 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('accessToken'),
        },
      })
      // console.log(response)
      dispatch(managerFormClose())
      dispatch(fetchManagersData())
      setTimeout(() => {
        dispatch(showAlert(response, SUCCESS_ALERT))
      }, 1000)
    } catch (error) {
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.log(error.response.data)
        console.log(error.response.status)
        console.log(error.response.headers)
        dispatch(showAlert(error.response, ERROR_ALERT))
      } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        console.log(error.request)
      } else {
        // Something happened in setting up the request that triggered an Error
        console.log('Error', error.message)
      }
      // console.log(error.config)
    }
  }
}

export function editManagerInfo(managerData) {
  debugger
  return async (dispatch) => {
    try {
      const response = await axios.put('/ManagersList', managerData, {
        headers: {
          Accept: 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('accessToken'),
        },
      })
      // console.log(response)
      dispatch(managerFormClose())
      dispatch(fetchManagersData())
      setTimeout(() => {
        dispatch(showAlert(response, SUCCESS_ALERT))
      }, 1000)
    } catch (error) {
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.log(error.response.data)
        console.log(error.response.status)
        console.log(error.response.headers)
        dispatch(showAlert(error.response, ERROR_ALERT))
      } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        console.log(error.request)
      } else {
        // Something happened in setting up the request that triggered an Error
        console.log('Error', error.message)
      }
      // console.log(error.config)
    }
  }
}

export function storeSelectedManagerData(managerData) {
  return {
    type: MANAGERS_STORE_SELECTED_MANAGER_DATA,
    payload: managerData,
  }
}

import axios from 'axios'
import {
  ERROR_ALERT,
  INFO_ALERT,
  SUCCESS_ALERT,
} from '../../components/Alerts/alertTypes'
import {
  CLIENTS_CLOSE_FORM,
  CLIENTS_FETCH_DATA,
  CLIENTS_FORM_FETCH_DATA,
  CLIENTS_GET_CONCRETE_CLIENT_DATA,
  CLIENTS_OPEN_FORM,
  CLIENTS_STORE_SELECTED_CLIENT_DATA,
  CONTACT_PERSONS_CLOSE_ADDFORM,
  CONTACT_PERSONS_FORM_FETCH_DATA,
  CONTACT_PERSONS_OPEN_ADDFORM,
  LOGOUT,
} from '../actionTypes'
import { showAlert } from './appActions'
import { fetchManagersData, managerFormClose } from './managersActions'
import { FORMTYPE_EDIT_CLIENT } from '../../components/ClientList/formTypes'

/*clients*/

export function fetchClientsData() {
  return async (dispatch) => {
    try {
      const response = await axios.get('/ClientsList', {
        headers: {
          Accept: 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('accessToken'),
        },
      })
      // console.log(response.data)
      dispatch({ type: CLIENTS_FETCH_DATA, payload: response.data })
    } catch (error) {
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.log(error.response.data)
        console.log(error.response.status)
        console.log(error.response.headers)
        if (error.response.status === 401) {
          dispatch({ type: LOGOUT })
          dispatch(showAlert(error.response, INFO_ALERT))
        }
        // dispatch(showAlert(error.response))
      } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        console.log(error.request)
      } else {
        // Something happened in setting up the request that triggered an Error
        console.log('Error', error.message)
      }
      // console.log(error.config)
    }
  }
}

// export function fetchConcreteClientData() {
//   return async (dispatch, getState) => {
//     const clientId = getState().clientsList.chosenClientId
//     if (clientId) {
//       try {
//         const response = await axios.get(`/ClientsList/${clientId}`, {
//           headers: {
//             Accept: 'application/json',
//             Authorization: 'Bearer ' + localStorage.getItem('accessToken'),
//           },
//         })
//         console.log(response.data)
//         dispatch({
//           type: CLIENTS_GET_CONCRETE_CLIENT_DATA,
//           payload: response.data,
//         })
//       } catch (error) {
//         if (error.response) {
//           // The request was made and the server responded with a status code
//           // that falls out of the range of 2xx
//           console.log(error.response.data)
//           console.log(error.response.status)
//           console.log(error.response.headers)
//           if (error.response.status === 401) {
//             dispatch({ type: LOGOUT })
//             dispatch(showAlert(error.response, INFO_ALERT))
//           }
//           // dispatch(showAlert(error.response))
//         } else if (error.request) {
//           // The request was made but no response was received
//           // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
//           // http.ClientRequest in node.js
//           console.log(error.request)
//         } else {
//           // Something happened in setting up the request that triggered an Error
//           console.log('Error', error.message)
//         }
//         // console.log(error.config)
//       }
//     } else {
//       return null
//     }
//   }
// }

export function clientFormOpen(
  formType,
  formTitle,
  formAlertText,
  formActionButton,
) {
  return {
    type: CLIENTS_OPEN_FORM,
    formType,
    formTitle,
    formAlertText,
    formActionButton,
  }
}

export function clientFormClose() {
  return {
    type: CLIENTS_CLOSE_FORM,
  }
}

function requestClientType() {
  return axios.get('/ClientTypes', {
    headers: {
      Accept: 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem('accessToken'),
    },
  })
}

function requestClientSources() {
  return axios.get('/ClientSources', {
    headers: {
      Accept: 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem('accessToken'),
    },
  })
}

export function clientsFormFetchData() {
  return async (dispatch) => {
    try {
      await Promise.all([requestClientType(), requestClientSources()]).then(
        (results) => {
          const clientTypes = results[0].data
          const clientSources = results[1].data
          dispatch({
            type: CLIENTS_FORM_FETCH_DATA,
            clientTypes,
            clientSources,
          })
        },
      )
    } catch (error) {
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.log(error.response.data)
        console.log(error.response.status)
        console.log(error.response.headers)
        if (error.response.status === 401) {
          dispatch({ type: LOGOUT })
          dispatch(showAlert(error.response, INFO_ALERT))
        }
        // dispatch(showAlert(error.response))
      } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        console.log(error.request)
      } else {
        // Something happened in setting up the request that triggered an Error
        console.log('Error', error.message)
      }
      // console.log(error.config)
    }
  }
}

export function addClientIntoDatabase(clientData) {
  return async (dispatch) => {
    try {
      const response = await axios.post('/ClientsList', clientData, {
        headers: {
          Accept: 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('accessToken'),
        },
      })
      // console.log(response)
      dispatch(clientFormClose())
      dispatch(fetchClientsData())
      setTimeout(() => {
        dispatch(showAlert(response, SUCCESS_ALERT))
      }, 1000)
    } catch (error) {
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.log(error.response.data)
        console.log(error.response.status)
        console.log(error.response.headers)
        dispatch(showAlert(error.response, ERROR_ALERT))
      } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        console.log(error.request)
      } else {
        // Something happened in setting up the request that triggered an Error
        console.log('Error', error.message)
      }
      // console.log(error.config)
    }
  }
}

export function storeSelectedClientData(clientData) {
  return {
    type: CLIENTS_STORE_SELECTED_CLIENT_DATA,
    payload: clientData,
  }
}

/*contact persons*/

export function contactPersonFormOpen() {
  return {
    type: CONTACT_PERSONS_OPEN_ADDFORM,
  }
}

export function contactPersonFormClose() {
  return {
    type: CONTACT_PERSONS_CLOSE_ADDFORM,
  }
}

export function addContactPersonToClient(contactPersonData) {
  return async (dispatch) => {
    try {
      const response = await axios.post('/ContactPersons', contactPersonData, {
        headers: {
          Accept: 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('accessToken'),
        },
      })
      console.log(response)
      dispatch(contactPersonFormClose())
      dispatch(fetchClientsData())
      setTimeout(() => {
        dispatch(showAlert(response, SUCCESS_ALERT))
      }, 1000)
    } catch (error) {
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.log(error.response.data)
        console.log(error.response.status)
        console.log(error.response.headers)
        dispatch(showAlert(error.response, ERROR_ALERT))
      } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        console.log(error.request)
      } else {
        // Something happened in setting up the request that triggered an Error
        console.log('Error', error.message)
      }
      // console.log(error.config)
    }
  }
}

export function contactPersonsFetchData() {
  return async (dispatch) => {
    try {
      const response = await axios.get('/PersonStatuses', {
        headers: {
          Accept: 'application/json',
          Authorization: 'Bearer ' + localStorage.getItem('accessToken'),
        },
      })
      console.log(response.data)
      dispatch({
        type: CONTACT_PERSONS_FORM_FETCH_DATA,
        payload: response.data,
      })
    } catch (error) {
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.log(error.response.data)
        console.log(error.response.status)
        console.log(error.response.headers)
        if (error.response.status === 401) {
          dispatch({ type: LOGOUT })
          dispatch(showAlert(error.response, INFO_ALERT))
        }
        // dispatch(showAlert(error.response))
      } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        console.log(error.request)
      } else {
        // Something happened in setting up the request that triggered an Error
        console.log('Error', error.message)
      }
      // console.log(error.config)
    }
  }
}

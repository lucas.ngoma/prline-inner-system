import axios from 'axios'
import { ERROR_ALERT } from '../../components/Alerts/alertTypes'
import { LOGIN } from './../actionTypes'
import { showAlert } from './appActions'

export function logIntoSystem(loginData) {
  return async (dispatch) => {
    try {
      const response = await axios.post('/login', loginData)
      console.log(response)
      localStorage.setItem('accessToken', response.data.access_token)
      localStorage.setItem('userType', response.data.role)
      dispatch({ type: LOGIN, payload: response.data })
    } catch (error) {
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.log(error.response.data)
        console.log(error.response.status)
        console.log(error.response.headers)
        dispatch(showAlert(error.response, ERROR_ALERT))
      } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        console.log(error.request)
      } else {
        // Something happened in setting up the request that triggered an Error
        console.log('Error', error.message)
      }
      // console.log(error.config)
    }
  }
}

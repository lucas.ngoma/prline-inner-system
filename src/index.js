import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import reportWebVitals from './reportWebVitals'
import axios from 'axios'
import { compose, createStore, applyMiddleware } from 'redux'
import thunk from 'redux-thunk'
import { Provider } from 'react-redux'
import { rootReducer } from './redux/Reducers/rootReducer'
import { ThemeProvider } from '@material-ui/core'
import { theme } from './theme/theme'
import { BrowserRouter, HashRouter, Route } from 'react-router-dom'
import { routes } from './routers/routes'
import { LoginPage } from './components/LoginPage/LoginPage'
import { ProtectedRoute } from './routers/ProtectedRoute'
import { Home } from './components/Home/Home'
import { ManagersList } from './components/ManagersList/ManagersList'
import { ClientsList } from './components/ClientList/ClientsList'
import { AdminSettings } from './components/AdminSettings/AdminSettings'
import { ContractorsList } from './components/ContractorsList/ContractorsList'

axios.defaults.baseURL = 'https://www.tariaagency.somee.com/api'

const store = createStore(
  rootReducer,
  compose(
    applyMiddleware(thunk),
    // @ts-ignore
    // window.__REDUX_DEVTOOLS_EXTENSION__ &&
    // @ts-ignore
    // window.__REDUX_DEVTOOLS_EXTENSION__(),
  ),
)

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <HashRouter>
          <Route path={routes.login} component={LoginPage} />
          <ProtectedRoute path={routes.home} component={Home} />
          <ProtectedRoute path={routes.managersList} component={ManagersList} />
          <ProtectedRoute path={routes.clientsList} component={ClientsList} />
          <ProtectedRoute
            path={routes.adminSettings}
            component={AdminSettings}
          />
          <ProtectedRoute
            path={routes.contractorsList}
            component={ContractorsList}
          />
          <App />
        </HashRouter>
      </ThemeProvider>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root'),
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()

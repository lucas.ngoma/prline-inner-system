import { createMuiTheme } from '@material-ui/core'

export const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#eb6008',
      light: '#f77626',
    },
    secondary: {
      main: '#005a87',
      light: '#0088cc',
    },
    inherit: {
      main: '#fff',
    },
    textPrimary: {
      main: '#fff',
      dark: '#000',
    },
  },
  transitions: {
    duration: {
      shortest: 150,
      shorter: 200,
      short: 250,
      standard: 300,
      complex: 375,
      enteringScreen: 225,
      leavingScreen: 195,
    },
  },
})

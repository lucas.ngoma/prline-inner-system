import React from 'react'
import { Redirect, Route } from 'react-router-dom'
import { routes } from './routes'
import { useSelector } from 'react-redux'
import { Navigation } from '../components/Navigation/Navigation'
import { Box, Container, Grid } from '@material-ui/core'
import { Header } from '../components/Header/Header'

export const ProtectedRoute = ({ component: Component, ...rest }) => {
  const authState = useSelector((state) => state.authentication)
  return (
    <Route
      {...rest}
      render={(props) => {
        if (authState.isAuth) {
          return (
            <div>
              <Header />
              <Box>
                <Container maxWidth='xl'>
                  <Grid container>
                    <Grid item xl={2} lg={2} md={3} sm={4}>
                      <Navigation />
                    </Grid>
                    <Grid item xl={10} lg={10} md={9} sm={8}>
                      <Component />
                    </Grid>
                  </Grid>
                </Container>
              </Box>
            </div>
          )
        } else {
          return (
            <Redirect to={{ pathname: routes.login, state: { from: props.location } }} />
          )
        }
      }}
    />
  )
}

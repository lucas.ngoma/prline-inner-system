export const routes = Object.freeze({
  root: '/',
  adminSettings: '/admin-settings',
  login: '/login',
  home: '/home',
  managersList: '/managers-list',
  clientsList: '/clients-list',
  contractorsList: '/contractors-list',
})

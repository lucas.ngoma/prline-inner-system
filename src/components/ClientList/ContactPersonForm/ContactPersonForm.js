import React, { useEffect } from 'react'
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  FormControlLabel,
  MenuItem,
  TextField,
} from '@material-ui/core'
import {
  addClientIntoDatabase,
  addContactPersonToClient,
  clientFormClose,
  clientsFormFetchData,
  contactPersonFormClose,
  contactPersonsFetchData,
} from '../../../redux/Actions/clientsActions'
import { Alert } from '@material-ui/lab'
import { useFormik } from 'formik'
import { FORMTYPE_ADD_CLIENT, FORMTYPE_EDIT_CLIENT } from '../formTypes'
import { useDispatch } from 'react-redux'
import Switch from '@material-ui/core/Switch'

export const ContactPersonForm = (props) => {
  useEffect(() => {
    dispatch(contactPersonsFetchData())
  }, [])
  const dispatch = useDispatch()
  const contactPersonFormik = useFormik({
    initialValues: props.initialValues,
    validationSchema: props.validationSchema,
    onSubmit: (values) => {
      values.isPrimary = Number(values.isPrimary)
      values.phone = Number(values.phone)
      debugger
      dispatch(addContactPersonToClient(values))
      console.log(values)
    },
    enableReinitialize: true,
  })

  return (
    <div>
      <Dialog
        open={props.clientsListState.contactPersonFormIsOpen}
        onClose={() => {
          dispatch(contactPersonFormClose())
        }}
        aria-labelledby='form-dialog-title'
      >
        <DialogTitle id='form-dialog-title'>Додати контактну особу</DialogTitle>
        <DialogContent>
          <DialogContentText>
            <Alert severity='warning'>
              <p>
                Зверніть увагу: Ви додаєте дані про контактну особу до клієнта{' '}
                <strong>{props.clientsListState.chosenClientName}</strong>
              </p>
            </Alert>
          </DialogContentText>
          <form onSubmit={contactPersonFormik.handleSubmit}>
            <TextField
              variant='outlined'
              margin='dense'
              id='lastName'
              label='Прізвище контактної особи'
              placeholder='Іванченко'
              type='text'
              fullWidth
              onChange={contactPersonFormik.handleChange}
              inputProps={{
                maxLength: 20,
              }}
              error={
                contactPersonFormik.errors.lastName &&
                contactPersonFormik.touched.lastName &&
                contactPersonFormik.errors.lastName
              }
              helperText={
                contactPersonFormik.errors.lastName &&
                contactPersonFormik.touched.lastName &&
                contactPersonFormik.errors.lastName
              }
            />
            <TextField
              variant='outlined'
              margin='dense'
              id='firstName'
              label="Ім'я контактної особи"
              placeholder='Іван'
              type='text'
              fullWidth
              onChange={contactPersonFormik.handleChange}
              inputProps={{
                maxLength: 20,
              }}
              error={
                contactPersonFormik.errors.firstName &&
                contactPersonFormik.touched.firstName &&
                contactPersonFormik.errors.firstName
              }
              helperText={
                contactPersonFormik.errors.firstName &&
                contactPersonFormik.touched.firstName &&
                contactPersonFormik.errors.firstName
              }
            />
            <TextField
              variant='outlined'
              margin='dense'
              id='middleName'
              label="Ім'я по батькові контактної особи"
              placeholder='Іванович'
              type='text'
              fullWidth
              onChange={contactPersonFormik.handleChange}
              inputProps={{
                maxLength: 20,
              }}
              error={
                contactPersonFormik.errors.middleName &&
                contactPersonFormik.touched.middleName &&
                contactPersonFormik.errors.middleName
              }
              helperText={
                contactPersonFormik.errors.middleName &&
                contactPersonFormik.touched.middleName &&
                contactPersonFormik.errors.middleName
              }
            />
            <TextField
              variant='outlined'
              margin='dense'
              id='phone'
              label='Номер телефону контактної особи'
              placeholder='Номер телефону контактної особи'
              type='text'
              fullWidth
              onChange={contactPersonFormik.handleChange}
              inputProps={{
                maxLength: 12,
              }}
              error={
                contactPersonFormik.errors.phone &&
                contactPersonFormik.touched.phone &&
                contactPersonFormik.errors.phone
              }
              helperText={
                contactPersonFormik.errors.phone &&
                contactPersonFormik.touched.phone &&
                contactPersonFormik.errors.phone
              }
            />
            <TextField
              variant='outlined'
              margin='dense'
              id='email'
              label='E-mail контактної особи'
              placeholder='E-mail контактної особи'
              type='text'
              fullWidth
              onChange={contactPersonFormik.handleChange}
              inputProps={{
                maxLength: 25,
              }}
              error={
                contactPersonFormik.errors.email &&
                contactPersonFormik.touched.email &&
                contactPersonFormik.errors.email
              }
              helperText={
                contactPersonFormik.errors.email &&
                contactPersonFormik.touched.email &&
                contactPersonFormik.errors.email
              }
            />
            <TextField
              variant='outlined'
              margin='dense'
              id='idPersonStatus'
              label='Тип контактної особи'
              select
              placeholder='Оберіть зі списку'
              type='text'
              fullWidth
              onChange={contactPersonFormik.handleChange('idPersonStatus')}
              value={contactPersonFormik.values.idPersonStatus}
            >
              {props.clientsListState.contactPersonStatuses
                ? props.clientsListState.contactPersonStatuses.map((option) => {
                    return (
                      <MenuItem key={option.id} value={option.id}>
                        {option.statusName}
                      </MenuItem>
                    )
                  })
                : null}
            </TextField>
            <FormControlLabel
              control={
                <Switch
                  checked={!!contactPersonFormik.values.isPrimary}
                  onChange={contactPersonFormik.handleChange}
                  color='primary'
                  name='isPrimary'
                />
              }
              label='Позначити як основну контактну особу'
            />

            <DialogActions>
              <Button
                onClick={() => {
                  dispatch(contactPersonFormClose())
                }}
                color='secondary'
              >
                Скасувати
              </Button>
              <Button color='primary' type='submit' variant='contained'>
                Додати
              </Button>
            </DialogActions>
          </form>
        </DialogContent>
      </Dialog>
    </div>
  )
}

import React from 'react'
import { useSelector } from 'react-redux'
import { ContactPersonForm } from './ContactPersonForm'
import * as Yup from 'yup'

export const ContactPersonFormContainer = () => {
  let clientsListState = useSelector((state) => state.clientsList)
  let personStatusesToOptions = useSelector(
    (state) => state.clientsList.contactPersonStatuses,
  )

  let initialValues = {
    firstName: '',
    lastName: '',
    middleName: '',
    phone: '',
    email: '',
    isPrimary: 0,
    idClient: clientsListState.chosenClientId,
    idPersonStatus: '',
  }

  let validationSchema = Yup.object({
    firstName: Yup.string()
      .matches(
        /^[А-Яа-яёЁЇїІіЄєҐґ]+$/,
        'Введіть згідно з форматом, наприклад: Іван',
      )
      .min(2, 'Мінімальна кількість символів - 2')
      .max(20, 'Максимальна кількість символів - 20')
      .required("Поле є обов'язковим"),
    lastName: Yup.string()
      .matches(
        /^[А-Яа-яёЁЇїІіЄєҐґ]+$/,
        'Введіть згідно з форматом, наприклад: Іванченко',
      )
      .min(2, 'Мінімальна кількість символів - 2')
      .max(20, 'Максимальна кількість символів - 20')
      .required("Поле є обов'язковим"),
    middleName: Yup.string()
      .matches(
        /^[А-Яа-яёЁЇїІіЄєҐґ]+$/,
        'Введіть згідно з форматом, наприклад: Іванович',
      )
      .min(2, 'Мінімальна кількість символів - 2')
      .max(25, 'Максимальна кількість символів - 25')
      .required("Поле є обов'язковим"),
    phone: Yup.string()
      .matches(
        /^\d{12}(\d{2})?$/,
        'Введіть згідно з форматом, наприклад: 380999999999',
      )
      // .typeError('Повинен містити лише цифри, наприклад 380999999999')
      .max(12, 'Максимальна кількість символів - 12')
      .required("Поле є обов'язковим"),
    email: Yup.string()
      .matches(
        /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/,
        'Введіть згідно з форматом, наприклад: example@example.com',
      )
      .max(25, 'Максимальна кількість символів - 25')
      .required("Поле є обов'язковим"),
  })

  return (
    <ContactPersonForm
      initialValues={initialValues}
      validationSchema={validationSchema}
      clientsListState={clientsListState}
      personStatusesToOptions={personStatusesToOptions}
    />
  )
}

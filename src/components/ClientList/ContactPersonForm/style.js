import { makeStyles } from '@material-ui/core/styles'
import { theme } from '../../../theme/theme'

export const useStyles = makeStyles({
  contactPersonsSectionAlert: {
    marginTop: '10px',
    marginBottom: '5px',
  },
})

import React from 'react'
import { DataGrid } from '@material-ui/data-grid'
import { useStyles } from './style'
import { Box, Fab, Typography } from '@material-ui/core'
import { Alert } from '@material-ui/lab'
import AddCircleIcon from '@material-ui/icons/AddCircle'
import GroupAddIcon from '@material-ui/icons/GroupAdd'
import EditIcon from '@material-ui/icons/Edit'
import { useDispatch, useSelector } from 'react-redux'
import { useEffect } from 'react'
import { SuccessAlert } from '../Alerts/SuccessAlert'
import { ErrorAlert } from '../Alerts/ErrorAlert'
import {
  ADD_CLIENT_FORM_ACTIONBUTTON,
  ADD_CLIENT_FORM_ALERT_TEXT,
  ADD_CLIENT_FORM_TITLE,
  EDIT_CLIENT_FORM_ACTIONBUTTON,
  EDIT_CLIENT_FORM_ALERT_TEXT,
  EDIT_CLIENT_FORM_TITLE,
  FORMTYPE_ADD_CLIENT,
  FORMTYPE_EDIT_CLIENT,
} from './formTypes'
import {
  clientFormOpen,
  contactPersonFormOpen,
  fetchClientsData,
  fetchConcreteClientData,
  storeSelectedClientData,
} from '../../redux/Actions/clientsActions'
import { ClientFormContainer } from './ClientForm/ClientFormContainer'
import { ContactPersonFormContainer } from './ContactPersonForm/ContactPersonFormContainer'
import { showAlert } from '../../redux/Actions/appActions'
import { ERROR_ALERT, SUCCESS_ALERT, WARNING_ALERT } from '../Alerts/alertTypes'
import { WarningAlert } from '../Alerts/WarningAlert'

const columns = [
  // { field: 'id', headerName: 'ID', width: 60 },
  { field: 'clientName', headerName: 'Клієнт', width: 250 },
  // { field: 'contactPerson', headerName: 'Контактна особа', width: 500 },
  { field: 'manager', headerName: 'Відповідальний менеджер', width: 250 },
  { field: 'ordersAmount', headerName: 'Кількість замовлень', width: 200 },
]

export const ClientsList = () => {
  useEffect(() => {
    dispatch(fetchClientsData())
  }, [])
  const dispatch = useDispatch()
  const classes = useStyles()
  const alertState = useSelector((state) => state.wholeApplication)
  let clientsListState = useSelector((state) => state.clientsList)
  let rows = clientsListState.clientsList
  return (
    <Box
      className={classes.contentContainer}
      display='flex'
      flexDirection='column'
      justifyContent='center'
    >
      <Typography variant='h5' gutterBottom>
        Список клієнтів
      </Typography>
      <SuccessAlert
        isOpen={alertState.successAlertIsOpen}
        alertCode={alertState.alertCode}
        alertText={alertState.alertText}
      />
      <ErrorAlert
        isOpen={alertState.errorAlertIsOpen}
        alertCode={alertState.alertCode}
        alertText={alertState.alertText}
      />
      <WarningAlert
        isOpen={alertState.warningAlertIsOpen}
        alertCode={alertState.alertCode}
        alertText={alertState.alertText}
      />
      <Alert severity='info'>
        Для того, щоб додати або редагувати клієнта, оберіть його у таблиці та
        натисніть відповідну кнопку.
      </Alert>
      <Box className={classes.dataGridContainer}>
        <DataGrid
          rows={rows}
          columns={columns}
          pageSize={10}
          loading={clientsListState.loading}
          onSelectionChange={(newSelection) => {
            for (let element of rows) {
              if (element.id === Number(newSelection.rowIds)) {
                dispatch(storeSelectedClientData(element))
                break
              }
            }
          }}
        />
      </Box>
      <Box
        className={classes.contentContainer}
        display='flex'
        flexDirection='row'
        justifyContent='flex-end'
      >
        <Fab
          className={classes.addClientButton}
          variant='extended'
          aria-label='addManager'
          onClick={() => {
            debugger
            dispatch(
              clientFormOpen(
                FORMTYPE_ADD_CLIENT,
                ADD_CLIENT_FORM_TITLE,
                ADD_CLIENT_FORM_ALERT_TEXT,
                ADD_CLIENT_FORM_ACTIONBUTTON,
              ),
            )
          }}
        >
          <AddCircleIcon />
          &nbsp; Додати
        </Fab>
        <Fab
          className={classes.editClientButton}
          variant='extended'
          aria-label='editManager'
          onClick={() => {
            debugger
            dispatch(
              clientFormOpen(
                FORMTYPE_EDIT_CLIENT,
                EDIT_CLIENT_FORM_TITLE,
                EDIT_CLIENT_FORM_ALERT_TEXT,
                EDIT_CLIENT_FORM_ACTIONBUTTON,
              ),
            )
            debugger
          }}
        >
          <EditIcon />
          &nbsp; Редагувати
        </Fab>
        <Fab
          className={`${classes.addContactPersonButton} ${classes.addContactPersonButton}`}
          variant='extended'
          aria-label='editManager'
          onClick={() => {
            if (clientsListState.chosenClientName) {
              dispatch(contactPersonFormOpen())
            } else if (!clientsListState.chosenClientName) {
              dispatch(
                showAlert(
                  {
                    data: {
                      infoText:
                        'Будь ласка, спочатку оберіть клієнта у таблиці',
                    },
                    status: 'Увага',
                  },
                  WARNING_ALERT,
                ),
              )
            }
          }}
        >
          <GroupAddIcon />
          &nbsp; Додати контактну особу
        </Fab>
      </Box>
      <ClientFormContainer />
      <ContactPersonFormContainer />
    </Box>
  )
}

export const FORMTYPE_ADD_CLIENT = 'FORMTYPE/addManagerForm'
export const FORMTYPE_EDIT_CLIENT = 'FORMTYPE/editManagerForm'

export const ADD_CLIENT_FORM_TITLE = 'Додати нового клієнта'
export const ADD_CLIENT_FORM_ALERT_TEXT =
  "Будь ласка, заповніть усі дані для того, щоб додати клієнта. Усі поля є обов'язковими."
export const ADD_CLIENT_FORM_ACTIONBUTTON = 'Додати'

export const EDIT_CLIENT_FORM_TITLE = 'Редагувати клієнта'

export const EDIT_CLIENT_FORM_ALERT_TEXT =
  'Зверніть увагу: якщо Вам не потрібно редагувати будь-яку інформацію, Ви можете просто залишити відповідне поле без змін.'
export const EDIT_CLIENT_FORM_ACTIONBUTTON = 'Редагувати'

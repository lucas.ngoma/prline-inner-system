import React, { useEffect, useState } from 'react'
import { useFormik } from 'formik'
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  MenuItem,
  TextField,
} from '@material-ui/core'
import { useDispatch } from 'react-redux'
import { Alert } from '@material-ui/lab'
import { FORMTYPE_ADD_CLIENT, FORMTYPE_EDIT_CLIENT } from '../formTypes'
import {
  addClientIntoDatabase,
  clientFormClose,
  clientsFormFetchData,
  fetchConcreteClientData,
} from '../../../redux/Actions/clientsActions'
import { useStyles } from './style'

export const ClientForm = (props) => {
  useEffect(() => {
    dispatch(clientsFormFetchData())
  }, [])
  // const classes = useStyles()
  const dispatch = useDispatch()

  const clientsFormik = useFormik({
    initialValues: props.initialValues,
    // validationSchema: props.validationSchema,
    onSubmit: (values) => {
      if (props.clientsListState.formToOpenType === FORMTYPE_ADD_CLIENT) {
        debugger
        values.code = Number(values.code)
        values.ipn = Number(values.ipn)
        values.legalPhone = Number(values.legalPhone)
        console.log(values)
        dispatch(addClientIntoDatabase(values))
      } else if (
        props.clientsListState.formToOpenType === FORMTYPE_EDIT_CLIENT
      ) {
        // values.enable = Number(values.enable)
        // console.log(values)
        // dispatch(editManagerInfo(values))
      }
    },
    enableReinitialize: true,
  })

  return (
    <div>
      <Dialog
        open={props.clientsListState.clientFormIsOpen}
        onClose={() => {
          dispatch(clientFormClose())
        }}
        aria-labelledby='form-dialog-title'
      >
        <DialogTitle id='form-dialog-title'>
          {props.clientsListState.formTitle}
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            <Alert severity='info'>
              {props.clientsListState.formAlertText}
            </Alert>
          </DialogContentText>
          <form onSubmit={clientsFormik.handleSubmit}>
            <TextField
              variant='outlined'
              margin='dense'
              id='idClientType'
              label='Тип клієнта'
              select
              placeholder='Оберіть зі списку'
              type='text'
              fullWidth
              onChange={clientsFormik.handleChange('idClientType')}
              value={clientsFormik.values.idClientType}
            >
              {props.clientsListState.clientTypes
                ? props.clientsListState.clientTypes.map((option) => {
                    return (
                      <MenuItem key={option.id} value={option.id}>
                        {option.typeName}
                      </MenuItem>
                    )
                  })
                : null}
            </TextField>
            <TextField
              variant='outlined'
              margin='dense'
              id='clientName'
              label="Ім'я або назва клієнта"
              placeholder='Наприклад: Іванов Іван Іванович, або назва установи'
              type='text'
              fullWidth
              onChange={clientsFormik.handleChange}
              value={clientsFormik.values.clientName}
              error={
                clientsFormik.errors.clientName &&
                clientsFormik.touched.clientName &&
                clientsFormik.errors.clientName
              }
              helperText={
                clientsFormik.errors.clientName &&
                clientsFormik.touched.clientName &&
                clientsFormik.errors.clientName
              }
            />
            <TextField
              variant='outlined'
              margin='dense'
              id='idClientSource'
              label='Джерело отриманого контакту'
              select
              placeholder='Оберіть зі списку'
              type='text'
              fullWidth
              readonly
              onChange={clientsFormik.handleChange('idClientSource')}
              value={clientsFormik.values.idClientSource}
              error={
                clientsFormik.errors.idClientSource &&
                clientsFormik.touched.idClientSource &&
                clientsFormik.errors.idClientSource
              }
              helperText={
                clientsFormik.errors.idClientSource &&
                clientsFormik.touched.idClientSource &&
                clientsFormik.errors.idClientSource
              }
            >
              {props.clientsListState.clientSources
                ? props.clientsListState.clientSources.map((option) => {
                    return (
                      <MenuItem key={option.id} value={option.id}>
                        {option.sourceName}
                      </MenuItem>
                    )
                  })
                : null}
            </TextField>
            <TextField
              variant='outlined'
              margin='dense'
              id='idUser'
              label='Відповідальний менеджер'
              select
              placeholder='Оберіть зі списку'
              type='text'
              fullWidth
              onChange={clientsFormik.handleChange('idUser')}
              value={clientsFormik.values.idUser}
              error={
                clientsFormik.errors.idUser &&
                clientsFormik.touched.idUser &&
                clientsFormik.errors.idUser
              }
              helperText={
                clientsFormik.errors.idUser &&
                clientsFormik.touched.idUser &&
                clientsFormik.errors.idUser
              }
            >
              {props.managersListToOptions
                ? props.managersListToOptions.map((option) => {
                    return (
                      <MenuItem key={option.id} value={option.id}>
                        {`${option.lastName} ${option.firstName} ${option.middleName}`}
                      </MenuItem>
                    )
                  })
                : null}
            </TextField>
            <TextField
              variant='outlined'
              margin='dense'
              id='address'
              label='Фактична адреса'
              placeholder='Наприклад: вул. Вулицька, д. 99, кв. 99'
              type='text'
              fullWidth
              onChange={clientsFormik.handleChange}
              value={clientsFormik.values.address}
              error={
                clientsFormik.errors.address &&
                clientsFormik.touched.address &&
                clientsFormik.errors.address
              }
              helperText={
                clientsFormik.errors.address &&
                clientsFormik.touched.address &&
                clientsFormik.errors.address
              }
            />
            <TextField
              variant='outlined'
              margin='dense'
              id='postAddress'
              label='Адреса для листування'
              placeholder='Адреса для листування'
              type='text'
              fullWidth
              onChange={clientsFormik.handleChange}
              value={clientsFormik.values.postAddress}
              error={
                clientsFormik.errors.postAddress &&
                clientsFormik.touched.postAddress &&
                clientsFormik.errors.postAddress
              }
              helperText={
                clientsFormik.errors.postAddress &&
                clientsFormik.touched.postAddress &&
                clientsFormik.errors.postAddress
              }
            />
            <TextField
              variant='outlined'
              margin='dense'
              id='site'
              label='Веб-сайт'
              placeholder='Наприклад: www.website.com'
              type='text'
              fullWidth
              onChange={clientsFormik.handleChange}
              value={clientsFormik.values.site}
              error={
                clientsFormik.errors.site &&
                clientsFormik.touched.site &&
                clientsFormik.errors.site
              }
              helperText={
                clientsFormik.errors.site &&
                clientsFormik.touched.site &&
                clientsFormik.errors.site
              }
            />
            <TextField
              variant='outlined'
              margin='dense'
              id='code'
              label='Код ЄДРПОУ'
              placeholder='Введіть код ЄДРПОУ'
              inputProps={{
                maxLength: 7,
              }}
              type='text'
              fullWidth
              onChange={clientsFormik.handleChange}
              value={clientsFormik.values.code}
              error={
                clientsFormik.errors.code &&
                clientsFormik.touched.code &&
                clientsFormik.errors.code
              }
              helperText={
                clientsFormik.errors.code &&
                clientsFormik.touched.code &&
                clientsFormik.errors.code
              }
            />
            <TextField
              variant='outlined'
              margin='dense'
              id='legalAddress'
              label='Юридична адреса'
              placeholder='Юридична адреса'
              type='text'
              fullWidth
              onChange={clientsFormik.handleChange}
              value={clientsFormik.values.legalAddress}
              error={
                clientsFormik.errors.legalAddress &&
                clientsFormik.touched.legalAddress &&
                clientsFormik.errors.legalAddress
              }
              helperText={
                clientsFormik.errors.legalAddress &&
                clientsFormik.touched.legalAddress &&
                clientsFormik.errors.legalAddress
              }
            />
            <TextField
              variant='outlined'
              margin='dense'
              id='legalPhone'
              label='Номер телефону'
              placeholder='Наприклад: 380999999999'
              inputProps={{
                maxLength: 12,
              }}
              type='text'
              fullWidth
              onChange={clientsFormik.handleChange}
              value={clientsFormik.values.legalPhone}
              error={
                clientsFormik.errors.legalPhone &&
                clientsFormik.touched.legalPhone &&
                clientsFormik.errors.legalPhone
              }
              helperText={
                clientsFormik.errors.legalPhone &&
                clientsFormik.touched.legalPhone &&
                clientsFormik.errors.legalPhone
              }
            />
            <TextField
              variant='outlined'
              margin='dense'
              id='iban'
              label='IBAN'
              placeholder='Введіть IBAN'
              inputProps={{
                maxLength: 30,
              }}
              type='text'
              fullWidth
              onChange={clientsFormik.handleChange}
              value={clientsFormik.values.iban}
              error={
                clientsFormik.errors.iban &&
                clientsFormik.touched.iban &&
                clientsFormik.errors.iban
              }
              helperText={
                clientsFormik.errors.iban &&
                clientsFormik.touched.iban &&
                clientsFormik.errors.iban
              }
            />
            <TextField
              variant='outlined'
              margin='dense'
              id='ipn'
              label='ІПН'
              placeholder='Введіть ІПН'
              inputProps={{
                maxLength: 9,
              }}
              type='text'
              fullWidth
              onChange={clientsFormik.handleChange}
              value={clientsFormik.values.ipn}
              error={
                clientsFormik.errors.ipn &&
                clientsFormik.touched.ipn &&
                clientsFormik.errors.ipn
              }
              helperText={
                clientsFormik.errors.ipn &&
                clientsFormik.touched.ipn &&
                clientsFormik.errors.ipn
              }
            />
            <TextField
              variant='outlined'
              margin='dense'
              id='director'
              label='Директор'
              placeholder='Директор'
              type='text'
              fullWidth
              onChange={clientsFormik.handleChange}
              value={clientsFormik.values.director}
              error={
                clientsFormik.errors.director &&
                clientsFormik.touched.director &&
                clientsFormik.errors.director
              }
              helperText={
                clientsFormik.errors.director &&
                clientsFormik.touched.director &&
                clientsFormik.errors.director
              }
            />
            {/*  <Alert
              className={classes.contactPersonsSectionAlert}
              severity='info'
              variant='outlined'
            >
              Дані про контактних осіб
            </Alert>
            <TextField
              variant='outlined'
              margin='dense'
              id='director'
              label="Ім'я контактної особи"
              placeholder='Іван'
              type='text'
              fullWidth
              onChange={clientsFormik.handleChange}
              // value={clientsFormik.values.director}
            />
            <TextField
              variant='outlined'
              margin='dense'
              id='director'
              label='Прізвище контактної особи'
              placeholder='Іванченко'
              type='text'
              fullWidth
              onChange={clientsFormik.handleChange}
              // value={clientsFormik.values.director}
            />
            <TextField
              variant='outlined'
              margin='dense'
              id='director'
              label="Ім'я по батькові контактної особи"
              placeholder='Іванович'
              type='text'
              fullWidth
              onChange={clientsFormik.handleChange}
              // value={clientsFormik.values.director}
            />
            <TextField
              variant='outlined'
              margin='dense'
              id='director'
              label='Номер телефону контактної особи'
              placeholder='Номер телефону контактної особи'
              type='text'
              fullWidth
              onChange={clientsFormik.handleChange}
              // value={clientsFormik.values.director}
            />
            <TextField
              variant='outlined'
              margin='dense'
              id='director'
              label='E-mail контактної особи'
              placeholder='E-mail контактної особи'
              type='text'
              fullWidth
              onChange={clientsFormik.handleChange}
              // value={clientsFormik.values.director}
            />*/}
            <TextField
              variant='outlined'
              margin='dense'
              id='notes'
              multiline
              rows={5}
              label='Нотатки'
              placeholder='Введіть текст'
              inputProps={{
                maxLength: 2000,
              }}
              type='text'
              fullWidth
              onChange={clientsFormik.handleChange}
              value={clientsFormik.values.notes}
              error={
                clientsFormik.errors.notes &&
                clientsFormik.touched.notes &&
                clientsFormik.errors.notes
              }
              helperText={
                clientsFormik.errors.notes &&
                clientsFormik.touched.notes &&
                clientsFormik.errors.notes
              }
            />
            <DialogActions>
              <Button
                onClick={() => {
                  dispatch(clientFormClose())
                }}
                color='secondary'
              >
                Скасувати
              </Button>
              <Button color='primary' type='submit' variant='contained'>
                {props.clientsListState.formActionButton}
              </Button>
            </DialogActions>
          </form>
        </DialogContent>
      </Dialog>
    </div>
  )
}

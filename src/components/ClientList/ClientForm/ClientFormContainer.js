import React, { useEffect } from 'react'
import { ClientForm } from './ClientForm'
import { useDispatch, useSelector } from 'react-redux'
import { FORMTYPE_EDIT_CLIENT } from '../formTypes'
import * as Yup from 'yup'
import {
  clientsFormFetchData,
  fetchConcreteClientData,
} from '../../../redux/Actions/clientsActions'

export const ClientFormContainer = () => {
  let clientsListState = useSelector((state) => state.clientsList)
  let managersListToOptions = useSelector(
    (state) => state.managersList.managersList,
  )

  let initialValues = {
    address: '',
    clientName: '',
    code: '',
    director: '',
    iban: '',
    idClientSource: '',
    idClientType: '',
    idUser: '',
    ipn: '',
    legalAddress: '',
    legalPhone: '',
    notes: '',
    postAddress: '',
    site: '',
  }

  // if (clientsListState.formToOpenType === FORMTYPE_EDIT_CLIENT) {
  //   initialValues = {
  //     address: clientsListState.concreteClientData.address || '',
  //     clientName: clientsListState.concreteClientData.clientName || '',
  //     code: clientsListState.concreteClientData.code || '',
  //     director: clientsListState.concreteClientData.director || '',
  //     iban: clientsListState.concreteClientData.iban || '',
  //     idClientSource: clientsListState.concreteClientData.idClientSource || '',
  //     idClientType: clientsListState.concreteClientData.idClientType || '',
  //     idUser: clientsListState.concreteClientData.idUser || '',
  //     ipn: clientsListState.concreteClientData.ipn || '',
  //     legalAddress: clientsListState.concreteClientData.legalAddress || '',
  //     legalPhone: clientsListState.concreteClientData.legalPhone || '',
  //     notes: clientsListState.concreteClientData.notes || '',
  //     postAddress: clientsListState.concreteClientData.postAddress || '',
  //     site: clientsListState.concreteClientData.site || '',
  //   }
  // }

  let validationSchema = Yup.object({
    clientName: Yup.string()
      // .matches(
      //   /^[A-Za-zА-Яа-яёЁЇїІіЄєҐґ]+$/,
      //   'Введіть згідно з форматом, наприклад: Іванов Іван Іванович, або назва установи',
      // )
      .max(100, 'Максимальна кількість символів - 80')
      .required("Поле є обов'язковим"),
    address: Yup.string()
      .max(100, 'Максимальна кількість символів - 100')
      .required("Поле є обов'язковим"),
    postAddress: Yup.string().max(100, 'Максимальна кількість символів - 100'),
    // .required("Поле є обов'язковим"),
    site: Yup.string().max(100, 'Максимальна кількість символів - 50'),
    // .required("Поле є обов'язковим"),
    idClientType: Yup.string().required("Поле є обов'язковим"),
    code: Yup.string()
      .matches(/([0-9])/, 'Дозволено вводити лише цифри')
      .max(8, 'Максимальна кількість символів - 8'),
    // .required("Поле є обов'язковим"),
    legalAddress: Yup.string().max(100, 'Максимальна кількість символів - 100'),
    // .required("Поле є обов'язковим"),
    legalPhone: Yup.string()
      .matches(
        /^\d{12}(\d{2})?$/,
        'Введіть згідно з форматом, наприклад: 380999999999',
      )
      .min(12, 'Мінімальна кількість символів - 12')
      .required("Поле є обов'язковим"),
    iban: Yup.string().max(30, 'Максимальна кількість символів - 30'),
    // .required("Поле є обов'язковим"),
    ipn: Yup.string().max(10, 'Максимальна кількість символів - 10'),
    // .required("Поле є обов'язковим"),
    director: Yup.string()
      .matches(
        /^[A-Za-zА-Яа-яёЁЇїІіЄєҐґ]+$/,
        'Введіть згідно з форматом, наприклад: Іванов Іван Іванович',
      )
      .max(10, 'Максимальна кількість символів - 10'),
    // .required("Поле є обов'язковим"),
    notes: Yup.string().max(2000, 'Максимальна кількість символів - 2000'),
    idClientSource: Yup.string().required("Поле є обов'язковим"),
    idClientUser: Yup.string().required("Поле є обов'язковим"),
  })

  return (
    <ClientForm
      clientsListState={clientsListState}
      initialValues={initialValues}
      validationSchema={validationSchema}
      managersListToOptions={managersListToOptions}
    />
  )
}

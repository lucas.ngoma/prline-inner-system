import { makeStyles } from '@material-ui/core/styles'
import { theme } from '../../theme/theme'

export const useStyles = makeStyles({
  contentContainer: {
    marginTop: '10px',
    paddingLeft: '10px',
  },
  dataGridContainer: {
    width: '100%',
    minHeight: '650px',
    marginTop: '10px',
  },
  addClientButton: {
    color: [[theme.palette.textPrimary.main], '!important'],
    backgroundColor: [[theme.palette.primary.light], '!important'],
    height: [[40], '!important'],
    minWidth: [[165], '!important'],
    marginRight: [[10], '!important'],
    '&:hover': {
      backgroundColor: [[theme.palette.primary.main], '!important'],
    },
  },
  editClientButton: {
    color: [[theme.palette.textPrimary.main], '!important'],
    backgroundColor: [[theme.palette.secondary.light], '!important'],
    height: [[40], '!important'],
    minWidth: [[165], '!important'],
    marginRight: [[10], '!important'],
    '&:hover': {
      backgroundColor: [[theme.palette.secondary.main], '!important'],
    },
  },
  addContactPersonButton: {
    color: theme.palette.textPrimary.main,
    backgroundColor: '#8BB174',
    height: 40,
    minWidth: 165,
    marginRight: 10,
    '&:hover': {
      backgroundColor: '#497a4a',
    },
  },
  deleteClientButton: {
    color: [[theme.palette.textPrimary.main], '!important'],
    backgroundColor: [[theme.palette.error.light], '!important'],
    height: [[40], '!important'],
    minWidth: [[165], '!important'],
    marginRight: [[10], '!important'],
    '&:hover': {
      backgroundColor: [[theme.palette.error.main], '!important'],
    },
  },
})

// export default useStyles

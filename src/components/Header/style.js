import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles({
  header: {
    backgroundColor: '#005a87',
    height: 75,
    color: '#fff',
  },
  muicontainer: {
    height: '100%',
  },
  welcomeSign: {
    marginRight: '25px',
  },
})

export default useStyles

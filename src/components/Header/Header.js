import React from 'react'
import { Box, Button, Container, Typography } from '@material-ui/core'
import useStyles from './style'
import { useDispatch, useSelector } from 'react-redux'
import { LOGOUT, SHOW_LOGOUT_DIALOG } from '../../redux/actionTypes'
import { LogOutDialog } from '../LogOutDialog/LogOutDialog'
import {
  hideLogOutDialog,
  showLogOutDialog,
} from '../../redux/Actions/appActions'

export const Header = () => {
  const classes = useStyles()
  const logOutWindowState = useSelector(
    (state) => state.wholeApplication.logOutDialogIsOpen,
  )
  const dispatch = useDispatch()
  return (
    <div>
      <header className={classes.header}>
        <Container className={classes.muicontainer} maxWidth='xl'>
          <Box
            height='100%'
            display='flex'
            justifyContent='space-between'
            alignItems='center'
          >
            <img
              src='https://pr-line.biz/wp-content/uploads/2020/06/logo.png'
              alt='PR-Line Logo'
            />
            <Box
              height='100%'
              display='flex'
              justifyContent='space-between'
              alignItems='center'
            >
              <Typography
                className={classes.welcomeSign}
                variant='h6'
                display='block'
              >
                Вітаємо, {localStorage.getItem('userType')}!
              </Typography>
              <Button
                variant='contained'
                size='small'
                color='primary'
                onClick={() => {
                  dispatch(showLogOutDialog())
                }}
              >
                Вихід
              </Button>
            </Box>
          </Box>
          <LogOutDialog isShown={logOutWindowState} />
        </Container>
      </header>
    </div>
  )
}

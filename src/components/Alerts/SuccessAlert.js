import React from 'react'
import { Alert } from '@material-ui/lab'
import { Snackbar } from '@material-ui/core'

import useStyles from './style'

export const SuccessAlert = ({
  isOpen = false,
  alertCode = undefined,
  alertText = undefined,
}) => {
  const classes = useStyles()
  return (
    <Snackbar open={isOpen}>
      <Alert variant='filled' severity='success'>
        {`${alertCode}: ${alertText}`}
      </Alert>
    </Snackbar>
  )
}

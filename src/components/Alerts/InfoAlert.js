import React from 'react'
import { Alert } from '@material-ui/lab'
import { Snackbar } from '@material-ui/core'

import useStyles from './style'

export const InfoAlert = ({
  isOpen = false,
  alertCode = undefined,
  alertText = undefined,
}) => {
  const classes = useStyles()
  return (
    <Snackbar open={isOpen}>
      <Alert variant='filled' severity='info'>
        {`${alertCode}: ${alertText}`}
      </Alert>
    </Snackbar>
  )
}

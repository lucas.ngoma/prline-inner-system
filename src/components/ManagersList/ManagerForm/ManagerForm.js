import React, { useState } from 'react'
import { useFormik } from 'formik'
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  FormControlLabel,
  IconButton,
  InputAdornment,
  TextField,
} from '@material-ui/core'
import VisibilitySharpIcon from '@material-ui/icons/VisibilitySharp'
import VisibilityOffSharpIcon from '@material-ui/icons/VisibilityOffSharp'
import { useDispatch } from 'react-redux'
import { Alert } from '@material-ui/lab'
import Switch from '@material-ui/core/Switch'
import { FORMTYPE_ADD_MANAGER, FORMTYPE_EDIT_MANAGER } from '../formTypes'
import {
  addManagerIntoDatabase,
  editManagerInfo,
  managerFormClose,
} from '../../../redux/Actions/managersActions'

export const ManagerForm = (props) => {
  const dispatch = useDispatch()
  const [showPassword, setShowPassword] = useState(false)
  const handleClickShowPassword = () => setShowPassword(!showPassword)

  const managerFormik = useFormik({
    initialValues: props.initialValues,
    validationSchema: props.validationSchema,
    onSubmit: (values, { resetForm }) => {
      if (props.managersListState.formToOpenType === FORMTYPE_ADD_MANAGER) {
        values.phone = Number(values.phone)
        dispatch(addManagerIntoDatabase(values))
        setTimeout(() => {
          resetForm({ values: '' })
        }, 3000)
      } else if (
        props.managersListState.formToOpenType === FORMTYPE_EDIT_MANAGER
      ) {
        values.enable = Number(values.enable)
        values.phone = Number(values.phone)
        dispatch(editManagerInfo(values))
      }
    },
    enableReinitialize: true,
  })

  return (
    <div>
      <Dialog
        open={props.managersListState.managerFormIsOpen}
        onClose={() => {
          dispatch(managerFormClose())
        }}
        aria-labelledby='form-dialog-title'
      >
        <DialogTitle id='form-dialog-title'>
          {props.managersListState.formTitle}
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            <Alert severity='info'>
              {props.managersListState.formAlertText}
            </Alert>
          </DialogContentText>
          <form onSubmit={managerFormik.handleSubmit}>
            <TextField
              autoFocus
              variant='outlined'
              margin='dense'
              id='email'
              label='E-mail'
              placeholder='example@example.com'
              inputProps={{
                maxLength: 25,
              }}
              fullWidth
              onChange={managerFormik.handleChange}
              value={managerFormik.values.email}
              error={
                managerFormik.errors.email &&
                managerFormik.touched.email &&
                managerFormik.errors.email
              }
              helperText={
                managerFormik.errors.email &&
                managerFormik.touched.email &&
                managerFormik.errors.email
              }
            />
            <TextField
              variant='outlined'
              margin='dense'
              id='password'
              label='Пароль'
              placeholder='Введіть пароль'
              type={showPassword ? 'text' : 'password'}
              inputProps={{
                maxLength: 20,
              }}
              fullWidth
              onChange={managerFormik.handleChange}
              value={managerFormik.values.password}
              InputProps={{
                endAdornment: (
                  <InputAdornment position='end'>
                    <IconButton
                      aria-label='toggle password visibility'
                      onClick={handleClickShowPassword}
                      edge='end'
                    >
                      {showPassword ? (
                        <VisibilitySharpIcon />
                      ) : (
                        <VisibilityOffSharpIcon />
                      )}
                    </IconButton>
                  </InputAdornment>
                ),
              }}
              error={
                managerFormik.errors.password &&
                managerFormik.touched.password &&
                managerFormik.errors.password
              }
              helperText={
                managerFormik.errors.password &&
                managerFormik.touched.password &&
                managerFormik.errors.password
              }
            />
            <TextField
              variant='outlined'
              margin='dense'
              id='firstName'
              label="Ім'я"
              placeholder='Іван'
              inputProps={{
                maxLength: 20,
              }}
              type='text'
              fullWidth
              onChange={managerFormik.handleChange}
              value={managerFormik.values.firstName}
              error={
                managerFormik.errors.firstName &&
                managerFormik.touched.firstName &&
                managerFormik.errors.firstName
              }
              helperText={
                managerFormik.errors.firstName &&
                managerFormik.touched.firstName &&
                managerFormik.errors.firstName
              }
            />
            <TextField
              variant='outlined'
              margin='dense'
              id='lastName'
              label='Прізвище'
              placeholder='Іванченко'
              type='text'
              inputProps={{
                maxLength: 20,
              }}
              fullWidth
              onChange={managerFormik.handleChange}
              value={managerFormik.values.lastName}
              error={
                managerFormik.errors.lastName &&
                managerFormik.touched.lastName &&
                managerFormik.errors.lastName
              }
              helperText={
                managerFormik.errors.lastName &&
                managerFormik.touched.lastName &&
                managerFormik.errors.lastName
              }
            />
            <TextField
              variant='outlined'
              margin='dense'
              id='middleName'
              label="Ім'я по батькові"
              placeholder='Іванович'
              type='text'
              inputProps={{
                maxLength: 20,
              }}
              fullWidth
              onChange={managerFormik.handleChange}
              value={managerFormik.values.middleName}
              error={
                managerFormik.errors.middleName &&
                managerFormik.touched.middleName &&
                managerFormik.errors.middleName
              }
              helperText={
                managerFormik.errors.middleName &&
                managerFormik.touched.middleName &&
                managerFormik.errors.middleName
              }
            />
            <TextField
              variant='outlined'
              margin='dense'
              id='phone'
              label='Номер телефону'
              placeholder='380509999999'
              type='tel'
              fullWidth
              inputProps={{
                maxLength: 12,
              }}
              onChange={managerFormik.handleChange}
              value={managerFormik.values.phone}
              error={
                managerFormik.errors.phone &&
                managerFormik.touched.phone &&
                managerFormik.errors.phone
              }
              helperText={
                managerFormik.errors.phone &&
                managerFormik.touched.phone &&
                managerFormik.errors.phone
              }
            />
            {props.managersListState.formToOpenType ===
            FORMTYPE_EDIT_MANAGER ? (
              <FormControlLabel
                control={
                  <Switch
                    checked={!!managerFormik.values.enable}
                    onChange={managerFormik.handleChange}
                    color='primary'
                    name='enable'
                  />
                }
                label='Активний'
              />
            ) : null}

            <DialogActions>
              <Button
                onClick={() => {
                  dispatch(managerFormClose())
                }}
                color='secondary'
              >
                Скасувати
              </Button>
              <Button color='primary' type='submit' variant='contained'>
                {props.managersListState.formActionButton}
              </Button>
            </DialogActions>
          </form>
        </DialogContent>
      </Dialog>
    </div>
  )
}

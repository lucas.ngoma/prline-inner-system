import React from 'react'
import { ManagerForm } from './ManagerForm'
import { useSelector } from 'react-redux'
import { FORMTYPE_ADD_MANAGER, FORMTYPE_EDIT_MANAGER } from '../formTypes'
import * as Yup from 'yup'

export const ManagerFormContainer = () => {
  let managersListState = useSelector((state) => state.managersList)
  let isRequired
  let initialValues = {
    lastName: managersListState.initialLastName,
    firstName: managersListState.initialFirstName,
    middleName: managersListState.initialMiddleName,
    email: managersListState.initialEmail,
    phone: managersListState.initialPhone,
    password: managersListState.initialPassword,
  }

  const requiredSchema = Yup.string().required("Поле є обов'язковим")

  if (managersListState.formToOpenType === FORMTYPE_EDIT_MANAGER) {
    initialValues.lastName = managersListState.chosenManagerLastName
    initialValues.firstName = managersListState.chosenManagerFirstName
    initialValues.middleName = managersListState.chosenManagerMiddleName
    initialValues.email = managersListState.chosenManagerEmailName
    initialValues.phone = managersListState.chosenManagerPhone
    initialValues.enable = managersListState.chosenManagerActive
    initialValues.id = managersListState.chosenManagerId
    isRequired = false
  } else if (managersListState.formToOpenType === FORMTYPE_ADD_MANAGER) {
    isRequired = true
  }

  let validationSchema = Yup.object({
    email: Yup.string()
      .matches(
        /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/,
        'Введіть згідно з форматом, наприклад: example@example.com',
      )
      .max(25, 'Максимальна кількість символів - 25')
      .required("Поле є обов'язковим"),
    password: Yup.string()
      .max(20, 'Максимальна кількість символів - 20')
      .concat(isRequired ? requiredSchema : null),
    firstName: Yup.string()
      .matches(
        /^[А-Яа-яёЁЇїІіЄєҐґ]+$/,
        'Введіть згідно з форматом, наприклад: Іван',
      )
      .min(2, 'Мінімальна кількість символів - 2')
      .max(20, 'Максимальна кількість символів - 20')
      .required("Поле є обов'язковим"),
    lastName: Yup.string()
      .matches(
        /^[А-Яа-яёЁЇїІіЄєҐґ]+$/,
        'Введіть згідно з форматом, наприклад: Іванченко',
      )
      .min(2, 'Мінімальна кількість символів - 2')
      .max(20, 'Максимальна кількість символів - 20')
      .required("Поле є обов'язковим"),
    middleName: Yup.string()
      .matches(
        /^[А-Яа-яёЁЇїІіЄєҐґ]+$/,
        'Введіть згідно з форматом, наприклад: Іванович',
      )
      .min(2, 'Мінімальна кількість символів - 2')
      .max(25, 'Максимальна кількість символів - 25')
      .required("Поле є обов'язковим"),
    phone: Yup.string()
      .matches(
        /^\d{12}(\d{2})?$/,
        'Введіть згідно з форматом, наприклад: 380999999999',
      )
      // .typeError('Повинен містити лише цифри, наприклад 380999999999')
      .max(12, 'Максимальна кількість символів - 12')
      .required("Поле є обов'язковим"),
  })

  return (
    <ManagerForm
      managersListState={managersListState}
      initialValues={initialValues}
      validationSchema={validationSchema}
    />
  )
}

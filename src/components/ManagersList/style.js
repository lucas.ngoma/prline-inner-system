import { makeStyles } from '@material-ui/core/styles'
import { theme } from '../../theme/theme'

export const useStyles = makeStyles({
  contentContainer: {
    marginTop: '10px',
    paddingLeft: '10px',
  },
  dataGridContainer: {
    width: '100%',
    minHeight: '650px',
    marginTop: '10px',
  },
  addManagerButton: {
    color: [[theme.palette.textPrimary.main], '!important'],
    backgroundColor: [[theme.palette.primary.light], '!important'],
    height: [[40], '!important'],
    width: [[165], '!important'],
    marginRight: [[10], '!important'],
    '&:hover': {
      backgroundColor: [[theme.palette.primary.main], '!important'],
    },
  },
  editManagerButton: {
    color: [[theme.palette.textPrimary.main], '!important'],
    backgroundColor: [[theme.palette.secondary.light], '!important'],
    height: [[40], '!important'],
    width: [[165], '!important'],
    marginRight: [[10], '!important'],
    '&:hover': {
      backgroundColor: [[theme.palette.secondary.main], '!important'],
    },
  },
  deleteManagerButton: {
    color: [[theme.palette.textPrimary.main], '!important'],
    backgroundColor: [[theme.palette.error.light], '!important'],
    height: [[40], '!important'],
    width: [[165], '!important'],
    marginRight: [[10], '!important'],
    '&:hover': {
      backgroundColor: [[theme.palette.error.main], '!important'],
    },
  },
})

// export default useStyles

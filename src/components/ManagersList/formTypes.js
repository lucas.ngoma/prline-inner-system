export const FORMTYPE_ADD_MANAGER = 'FORMTYPE/addManagerForm'
export const FORMTYPE_EDIT_MANAGER = 'FORMTYPE/editManagerForm'

export const ADD_MANAGER_FORM_TITLE = 'Додати нового менеджера'
export const ADD_MANAGER_FORM_ALERT_TEXT =
  "Будь ласка, заповніть усі дані для того, щоб додати менеджера. Усі поля є обов'язковими."
export const ADD_MANAGER_FORM_ACTIONBUTTON = 'Додати'

export const EDIT_MANAGER_FORM_TITLE = 'Редагувати менеджера'

export const EDIT_MANAGER_FORM_ALERT_TEXT =
  'Зверніть увагу: якщо Вам не потрібно редагувати будь-яку інформацію, Ви можете просто залишити відповідне поле без змін. Якщо редагувати пароль не потрібно — залишайте поле пустим.'
export const EDIT_MANAGER_FORM_ACTIONBUTTON = 'Редагувати'

import React from 'react'
import { DataGrid } from '@material-ui/data-grid'
import { useStyles } from './style'
import { Box, Fab, Typography } from '@material-ui/core'
import { Alert } from '@material-ui/lab'
import AddCircleIcon from '@material-ui/icons/AddCircle'
import EditIcon from '@material-ui/icons/Edit'
import { useDispatch, useSelector } from 'react-redux'
import { useEffect } from 'react'
import {
  fetchManagersData,
  managerFormOpen,
  storeSelectedManagerData,
} from '../../redux/Actions/managersActions'
import { SuccessAlert } from '../Alerts/SuccessAlert'
import { ErrorAlert } from '../Alerts/ErrorAlert'
import {
  ADD_MANAGER_FORM_ACTIONBUTTON,
  ADD_MANAGER_FORM_ALERT_TEXT,
  ADD_MANAGER_FORM_TITLE,
  EDIT_MANAGER_FORM_ACTIONBUTTON,
  EDIT_MANAGER_FORM_ALERT_TEXT,
  EDIT_MANAGER_FORM_TITLE,
  FORMTYPE_ADD_MANAGER,
  FORMTYPE_EDIT_MANAGER,
} from './formTypes'
import { ManagerFormContainer } from './ManagerForm/ManagerFormContainer'
import { showAlert } from '../../redux/Actions/appActions'
import { WARNING_ALERT } from '../Alerts/alertTypes'
import { WarningAlert } from '../Alerts/WarningAlert'
import { ForbiddenPage } from '../ForbiddenPage/ForbidenPage'
import { storeSelectedClientData } from '../../redux/Actions/clientsActions'

const columns = [
  // { field: 'id', headerName: 'ID', width: 60 },
  { field: 'lastName', headerName: 'Прізвище', width: 150 },
  { field: 'firstName', headerName: "Ім'я", width: 150 },
  { field: 'middleName', headerName: 'По батькові', width: 150 },
  { field: 'email', headerName: 'Email', width: 200 },
  {
    field: 'phone',
    headerName: 'Мобільний телефон',
    width: 170,
  },
  { field: 'enable', headerName: 'Активний', width: 100 },
]

export const ManagersList = () => {
  const userType = localStorage.getItem('userType')
  useEffect(() => {
    dispatch(fetchManagersData())
  }, [])
  const dispatch = useDispatch()
  const classes = useStyles()
  const alertState = useSelector((state) => state.wholeApplication)
  let managersListState = useSelector((state) => state.managersList)

  let rows = managersListState.managersList
  if (userType !== 'admin') {
    return <ForbiddenPage />
  } else {
    return (
      <Box
        className={classes.contentContainer}
        display='flex'
        flexDirection='column'
        justifyContent='center'
      >
        <Typography variant='h5' gutterBottom>
          Список менеджерів
        </Typography>
        <SuccessAlert
          isOpen={alertState.successAlertIsOpen}
          alertCode={alertState.alertCode}
          alertText={alertState.alertText}
        />
        <ErrorAlert
          isOpen={alertState.errorAlertIsOpen}
          alertCode={alertState.alertCode}
          alertText={alertState.alertText}
        />
        <WarningAlert
          isOpen={alertState.warningAlertIsOpen}
          alertCode={alertState.alertCode}
          alertText={alertState.alertText}
        />
        <Alert severity='info'>
          Для того, щоб редагувати або видалити менеджера, оберіть його у
          таблиці та натисніть відповідну кнопку.
        </Alert>
        <Box className={classes.dataGridContainer}>
          <DataGrid
            rows={rows}
            columns={columns}
            pageSize={10}
            loading={managersListState.loading}
            onSelectionChange={(newSelection) => {
              for (let element of rows) {
                if (element.id === Number(newSelection.rowIds)) {
                  dispatch(storeSelectedManagerData(element))
                  break
                }
              }
            }}
          />
        </Box>
        <Box
          className={classes.contentContainer}
          display='flex'
          flexDirection='row'
          justifyContent='flex-end'
        >
          <Fab
            className={`${classes.addManagerButton} ${classes.addManagerButton}`}
            variant='extended'
            aria-label='addManager'
            onClick={() => {
              dispatch(
                managerFormOpen(
                  FORMTYPE_ADD_MANAGER,
                  ADD_MANAGER_FORM_TITLE,
                  ADD_MANAGER_FORM_ALERT_TEXT,
                  ADD_MANAGER_FORM_ACTIONBUTTON,
                ),
              )
            }}
          >
            <AddCircleIcon />
            &nbsp; Додати
          </Fab>
          <Fab
            className={classes.editManagerButton}
            variant='extended'
            aria-label='editManager'
            onClick={() => {
              if (managersListState.chosenManagerFirstName) {
                dispatch(
                  managerFormOpen(
                    FORMTYPE_EDIT_MANAGER,
                    EDIT_MANAGER_FORM_TITLE,
                    EDIT_MANAGER_FORM_ALERT_TEXT,
                    EDIT_MANAGER_FORM_ACTIONBUTTON,
                  ),
                )
              } else if (!managersListState.chosenManagerFirstName) {
                dispatch(
                  showAlert(
                    {
                      data: {
                        infoText:
                          'Будь ласка, спочатку оберіть менеджера у таблиці',
                      },
                      status: 'Увага',
                    },
                    WARNING_ALERT,
                  ),
                )
              }
            }}
          >
            <EditIcon />
            &nbsp; Редагувати
          </Fab>
          {/*<Fab*/}
          {/*  className={classes.deleteManagerButton}*/}
          {/*  variant='extended'*/}
          {/*  aria-label='deleteManager'*/}
          {/*>*/}
          {/*  <RemoveCircleIcon />*/}
          {/*  &nbsp; Видалити*/}
          {/*</Fab>*/}
        </Box>
        <ManagerFormContainer />
      </Box>
    )
  }
}

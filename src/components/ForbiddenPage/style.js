import { makeStyles } from '@material-ui/core/styles'
import { theme } from '../../theme/theme'

export const useStyles = makeStyles({
  container: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  welcomeSign: {
    textAlign: 'center',
    color: theme.palette.secondary.light,
  },
  welcomeSignError: {
    color: theme.palette.primary.light,
  },
  forbiddenMessage: {
    textAlign: 'center',
    color: theme.palette.secondary.light,
    marginTop: '12px',
  },
})

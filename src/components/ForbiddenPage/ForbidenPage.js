import React from 'react'
import Container from '@material-ui/core/Container'
import { useStyles } from './style'

export const ForbiddenPage = () => {
  const classes = useStyles()
  return (
    <Container
      className={`${classes.container} ${classes.container}`}
      maxWidth='md'
    >
      <h1 className={`${classes.welcomeSign} ${classes.welcomeSign}`}>
        ЛАСКАВО ПРОСИМО ДО СТОРІНКИ{' '}
        <span
          className={`${classes.welcomeSignError} ${classes.welcomeSignError}`}
        >
          403
        </span>{' '}
        :)
      </h1>
      <p className={`${classes.forbiddenMessage} ${classes.forbiddenMessage}`}>
        У вас недостатньо прав для перегляду цього ресурсу, зверніться до
        адміністратора.
      </p>
    </Container>
  )
}

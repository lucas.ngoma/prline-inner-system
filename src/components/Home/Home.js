import { Container, Grid } from '@material-ui/core'
import React from 'react'
import { Route, Switch } from 'react-router-dom'
import { ProtectedRoute } from '../../routers/ProtectedRoute'
import { routes } from '../../routers/routes'
import { Header } from '../Header/Header'
import { ManagersList } from '../ManagersList/ManagersList'
import { Navigation } from '../Navigation/Navigation'

export const Home = () => {
  return (
    <div>
      <h1>home</h1>
    </div>
  )
}

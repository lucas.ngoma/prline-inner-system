import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles({
  form: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  formInput: {
    marginTop: 20,
    background: '#fff',
    width: 300,
  },
  formSubmit: {
    width: 150,
    marginTop: 20,
    backgroundColor: '#eb6008',
    transition: 'ease all .3s',
  },
})

export default useStyles

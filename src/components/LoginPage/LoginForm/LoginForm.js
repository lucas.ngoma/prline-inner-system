import React, { useState } from 'react'
import { useFormik } from 'formik'
import {
  TextField,
  Button,
  InputAdornment,
  IconButton,
} from '@material-ui/core'
import { Grow } from '@material-ui/core'
import VisibilitySharpIcon from '@material-ui/icons/VisibilitySharp'
import VisibilityOffSharpIcon from '@material-ui/icons/VisibilityOffSharp'
import { useDispatch } from 'react-redux'
import { logIntoSystem } from '../../../redux/Actions/loginActions'
import useStyles from './style'

export const LoginForm = () => {
  const classes = useStyles()
  const dispatch = useDispatch()
  const [showPassword, setShowPassword] = useState(false)
  const handleClickShowPassword = () => setShowPassword(!showPassword)
  const formik = useFormik({
    // adminUser qwerty1234
    initialValues: {
      username: '',
      password: '',
    },
    onSubmit: (values) => {
      debugger
      dispatch(logIntoSystem(values))
    },
  })
  return (
    <form className={classes.form} onSubmit={formik.handleSubmit}>
      <Grow in timeout={400}>
        <TextField
          required
          value={formik.values.username}
          className={classes.formInput}
          id='username'
          label="Ім'я користувача"
          variant='filled'
          onChange={formik.handleChange}
        />
      </Grow>
      <Grow in timeout={600}>
        <TextField
          required
          value={formik.values.password}
          className={classes.formInput}
          type={showPassword ? 'text' : 'password'}
          id='password'
          label='Пароль'
          variant='filled'
          onChange={formik.handleChange}
          InputProps={{
            endAdornment: (
              <InputAdornment position='end'>
                <IconButton
                  aria-label='toggle password visibility'
                  onClick={handleClickShowPassword}
                  // onMouseDown={handleMouseDownPassword}
                  edge='end'
                >
                  {showPassword ? (
                    <VisibilitySharpIcon />
                  ) : (
                    <VisibilityOffSharpIcon />
                  )}
                </IconButton>
              </InputAdornment>
            ),
          }}
        />
      </Grow>
      <Grow in timeout={800}>
        <Button
          className={classes.formSubmit}
          type='submit'
          variant='contained'
          color='primary'
          size='large'
        >
          Увійти
        </Button>
      </Grow>
    </form>
  )
}

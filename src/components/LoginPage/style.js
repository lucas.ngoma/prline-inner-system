import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles({
  wrapper: {
    backgroundColor: '#005a87',
  },
  container: {
    height: '100vh',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
})

export default useStyles

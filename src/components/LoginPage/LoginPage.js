import React from 'react'
import { Container } from '@material-ui/core'
import { Grow } from '@material-ui/core'
import { useSelector } from 'react-redux'
import { LoginForm } from './LoginForm/LoginForm'
import useStyles from './style'
import { ErrorAlert } from '../Alerts/ErrorAlert'
import { InfoAlert } from '../Alerts/InfoAlert'

export const LoginPage = () => {
  const classes = useStyles()
  const alertState = useSelector((state) => state.wholeApplication)

  return (
    <div className={classes.wrapper}>
      <ErrorAlert
        isOpen={alertState.errorAlertIsOpen}
        alertCode={alertState.alertCode}
        alertText={alertState.alertText}
      />
      <InfoAlert
        isOpen={alertState.infoAlertIsOpen}
        alertCode={alertState.alertCode}
        alertText={alertState.alertText}
      />
      <Container className={classes.container}>
        <Grow in timeout={400}>
          <img
            src='https://pr-line.biz/wp-content/uploads/2020/06/logo.png'
            alt='PR-Line Logo'
          />
        </Grow>
        <LoginForm />
      </Container>
    </div>
  )
}

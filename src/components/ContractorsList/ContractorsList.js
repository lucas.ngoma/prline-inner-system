import React, { useEffect } from 'react'
import {
  clientFormOpen,
  contactPersonFormOpen,
  fetchClientsData,
  storeSelectedClientData,
} from '../../redux/Actions/clientsActions'
import { useDispatch, useSelector } from 'react-redux'
import { useStyles } from '../ClientList/style'
import { Box, Fab, Typography } from '@material-ui/core'
import { SuccessAlert } from '../Alerts/SuccessAlert'
import { ErrorAlert } from '../Alerts/ErrorAlert'
import { WarningAlert } from '../Alerts/WarningAlert'
import { Alert } from '@material-ui/lab'
import { DataGrid } from '@material-ui/data-grid'
import {
  ADD_CLIENT_FORM_ACTIONBUTTON,
  ADD_CLIENT_FORM_ALERT_TEXT,
  ADD_CLIENT_FORM_TITLE,
  EDIT_CLIENT_FORM_ACTIONBUTTON,
  EDIT_CLIENT_FORM_ALERT_TEXT,
  EDIT_CLIENT_FORM_TITLE,
  FORMTYPE_ADD_CLIENT,
  FORMTYPE_EDIT_CLIENT,
} from '../ClientList/formTypes'
import AddCircleIcon from '@material-ui/icons/AddCircle'
import EditIcon from '@material-ui/icons/Edit'
import { showAlert } from '../../redux/Actions/appActions'
import { WARNING_ALERT } from '../Alerts/alertTypes'
import GroupAddIcon from '@material-ui/icons/GroupAdd'
import { ClientFormContainer } from '../ClientList/ClientForm/ClientFormContainer'
import { ContactPersonFormContainer } from '../ClientList/ContactPersonForm/ContactPersonFormContainer'
import { fetchContractorsData } from '../../redux/Actions/contractorsActions'
import PersonPinIcon from '@material-ui/icons/PersonPin'

const columns = [
  // { field: 'id', headerName: 'ID', width: 60 },
  { field: 'contractorName', headerName: 'Назва підрядника', width: 250 },
  { field: 'address', headerName: 'Адреса підрядника (фактична)', width: 300 },
  { field: 'site', headerName: 'Веб-сайт', width: 250 },
  { field: 'activities', headerName: 'Діяльність', width: 300 },
]

export const ContractorsList = () => {
  const dispatch = useDispatch()
  useEffect(() => {
    dispatch(fetchContractorsData())
  }, [])
  // const dispatch = useDispatch()
  const classes = useStyles()
  const alertState = useSelector((state) => state.wholeApplication)
  let contractorsListState = useSelector((state) => state.contractorsList) //CONTRACTORS STATE
  let rows = contractorsListState.contractorsList
  return (
    <Box
      className={classes.contentContainer}
      display='flex'
      flexDirection='column'
      justifyContent='center'
    >
      <Typography variant='h5' gutterBottom>
        Список підрядників
      </Typography>
      <SuccessAlert
        isOpen={alertState.successAlertIsOpen}
        alertCode={alertState.alertCode}
        alertText={alertState.alertText}
      />
      <ErrorAlert
        isOpen={alertState.errorAlertIsOpen}
        alertCode={alertState.alertCode}
        alertText={alertState.alertText}
      />
      <WarningAlert
        isOpen={alertState.warningAlertIsOpen}
        alertCode={alertState.alertCode}
        alertText={alertState.alertText}
      />
      <Alert severity='info'>
        Для того, щоб додати або редагувати підрядника, оберіть його у таблиці
        та натисніть відповідну кнопку.
      </Alert>
      <Box className={classes.dataGridContainer}>
        <DataGrid
          rows={rows}
          columns={columns}
          pageSize={10}
          loading={contractorsListState.loading}
          onSelectionChange={(newSelection) => {
            for (let element of rows) {
              if (element.id === Number(newSelection.rowIds)) {
                //selection logic
              }
            }
          }}
        />
      </Box>
      {/*<Box*/}
      {/*  className={classes.contentContainer}*/}
      {/*  display='flex'*/}
      {/*  flexDirection='row'*/}
      {/*  justifyContent='flex-end'*/}
      {/*>*/}
      {/*  <Fab*/}
      {/*    className={classes.addClientButton}*/}
      {/*    variant='extended'*/}
      {/*    aria-label='addManager'*/}
      {/*    onClick={() => {*/}
      {/*      debugger*/}
      {/*      dispatch(*/}
      {/*        clientFormOpen(*/}
      {/*          FORMTYPE_ADD_CLIENT,*/}
      {/*          ADD_CLIENT_FORM_TITLE,*/}
      {/*          ADD_CLIENT_FORM_ALERT_TEXT,*/}
      {/*          ADD_CLIENT_FORM_ACTIONBUTTON,*/}
      {/*        ),*/}
      {/*      )*/}
      {/*    }}*/}
      {/*  >*/}
      {/*    <AddCircleIcon />*/}
      {/*    &nbsp; Додати*/}
      {/*  </Fab>*/}
      {/*  <Fab*/}
      {/*    className={classes.editClientButton}*/}
      {/*    variant='extended'*/}
      {/*    aria-label='editManager'*/}
      {/*    onClick={() => {*/}
      {/*      debugger*/}
      {/*      dispatch(*/}
      {/*        clientFormOpen(*/}
      {/*          FORMTYPE_EDIT_CLIENT,*/}
      {/*          EDIT_CLIENT_FORM_TITLE,*/}
      {/*          EDIT_CLIENT_FORM_ALERT_TEXT,*/}
      {/*          EDIT_CLIENT_FORM_ACTIONBUTTON,*/}
      {/*        ),*/}
      {/*      )*/}
      {/*      debugger*/}
      {/*    }}*/}
      {/*  >*/}
      {/*    <EditIcon />*/}
      {/*    &nbsp; Редагувати*/}
      {/*  </Fab>*/}
      {/*  <Fab*/}
      {/*    className={`${classes.addContactPersonButton} ${classes.addContactPersonButton}`}*/}
      {/*    variant='extended'*/}
      {/*    aria-label='editManager'*/}
      {/*    onClick={() => {*/}
      {/*      if (clientsListState.chosenClientName) {*/}
      {/*        dispatch(contactPersonFormOpen())*/}
      {/*      } else if (!clientsListState.chosenClientName) {*/}
      {/*        dispatch(*/}
      {/*          showAlert(*/}
      {/*            {*/}
      {/*              data: {*/}
      {/*                infoText:*/}
      {/*                  'Будь ласка, спочатку оберіть клієнта у таблиці',*/}
      {/*              },*/}
      {/*              status: 'Увага',*/}
      {/*            },*/}
      {/*            WARNING_ALERT,*/}
      {/*          ),*/}
      {/*        )*/}
      {/*      }*/}
      {/*    }}*/}
      {/*  >*/}
      {/*    <GroupAddIcon />*/}
      {/*    &nbsp; Додати контактну особу*/}
      {/*  </Fab>*/}
      {/*</Box>*/}
    </Box>
  )
}

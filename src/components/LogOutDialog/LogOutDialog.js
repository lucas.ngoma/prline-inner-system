import React from 'react'
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
} from '@material-ui/core'
import { useDispatch } from 'react-redux'
import { hideLogOutDialog } from '../../redux/Actions/appActions'
import { LOGOUT } from '../../redux/actionTypes'

export const LogOutDialog = (props) => {
  const dispatch = useDispatch()
  return (
    <Dialog
      open={props.isShown}
      onClose={() => {
        dispatch(hideLogOutDialog())
      }}
      aria-labelledby='logout-dialog-title'
      aria-describedby='logout-description'
    >
      <DialogTitle id='logout-dialog-title'>{'Вийти з системи?'}</DialogTitle>
      <DialogContent>
        <DialogContentText id='logout-description'>
          Збережіть усі зміни перед виходом.
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button
          onClick={() => {
            dispatch(hideLogOutDialog())
          }}
          color='secondary'
        >
          Скасувати
        </Button>
        <Button
          onClick={() => {
            dispatch(hideLogOutDialog())
            dispatch({ type: LOGOUT })
          }}
          variant='contained'
          color='primary'
          autoFocus
        >
          Вийти
        </Button>
      </DialogActions>
    </Dialog>
  )
}

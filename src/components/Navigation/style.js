import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles({
  navBar: {
    width: '100%',
  },
})

export default useStyles

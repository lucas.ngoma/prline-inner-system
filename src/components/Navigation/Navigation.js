import React from 'react'
import { List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core'
import ContactsIcon from '@material-ui/icons/Contacts'
import PeopleIcon from '@material-ui/icons/People'
import PersonPinIcon from '@material-ui/icons/PersonPin'
import HelpIcon from '@material-ui/icons/Help'
import SettingsIcon from '@material-ui/icons/Settings'
import { NavLink } from 'react-router-dom'
import { routes } from '../../routers/routes'
import useStyles from './style'
import { useDispatch } from 'react-redux'
import { storeCurrentPageRoute } from '../../redux/Actions/appActions'
import { ContractorsList } from '../ContractorsList/ContractorsList'

export const Navigation = () => {
  const userType = localStorage.getItem('userType')
  const classes = useStyles()
  return (
    <List component='nav' className={classes.navBar}>
      {userType === 'admin' ? (
        <ListItem
          button
          key='ManagersList'
          component={NavLink}
          to={routes.managersList}
        >
          <ListItemIcon>
            <ContactsIcon />
          </ListItemIcon>
          <ListItemText primary='Список менеджерів' />
        </ListItem>
      ) : null}
      <ListItem
        button
        key='ClientsList'
        component={NavLink}
        to={routes.clientsList}
      >
        <ListItemIcon>
          <PeopleIcon />
        </ListItemIcon>
        <ListItemText primary='Список клієнтів' />
      </ListItem>
      <ListItem
        button
        key='ContractorsList'
        component={NavLink}
        to={routes.contractorsList}
      >
        <ListItemIcon>
          <PersonPinIcon />
        </ListItemIcon>
        <ListItemText primary='Список підрядників' />
      </ListItem>

      <ListItem
        button
        key='AdminSettings'
        component={NavLink}
        to={routes.adminSettings}
      >
        <ListItemIcon>
          <SettingsIcon />
        </ListItemIcon>
        <ListItemText primary='Налаштування системи' />
      </ListItem>
    </List>
  )
}

import React from 'react'
import { useSelector } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { routes } from '../routers/routes'

export const authRedirect = (Component) => {
  const authState = useSelector((state) => state.authentication)
  const RedirectComponent = () => {
    if (authState.isAuth) {
      return <Redirect to={routes.home} />
    } else {
      return <Component />
    }
  }
  return RedirectComponent
}

import React from 'react'
import { Redirect } from 'react-router-dom'
import { useSelector } from 'react-redux'
import './App.css'
import { routes } from './routers/routes'

function App() {
  const userType = localStorage.getItem('userType')
  const authState = useSelector((state) => state.authentication)
  if (authState.isAuth && userType === 'admin') {
    return <Redirect to={routes.managersList} />
  } else if (authState.isAuth && userType === 'manager') {
    return <Redirect to={routes.clientsList} />
  } else {
    return <Redirect to={routes.login} />
  }
}

export default App
